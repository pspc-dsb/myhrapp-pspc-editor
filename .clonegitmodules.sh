#!/bin/bash
git submodule add https://gitlab.com/pspc-dsb/my-hr-app-pspc-content-english.git source/en
git submodule add https://gitlab.com/pspc-dsb/my-hr-app-pspc-content-french.git source/fr
git submodule sync --recursive
