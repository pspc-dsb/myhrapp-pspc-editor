#!/bin/bash
export DOCKER_BUILDKIT=1
docker login registry.gitlab.com
docker build \
    -t registry.gitlab.com/pspc-dsb/myhrapp-pspc-editor/middleman \
    --build-arg RUBY_VERSION=$(cat .ruby-version) \
    .
docker push registry.gitlab.com/pspc-dsb/myhrapp-pspc-editor/middleman
