ARG RUBY_VERSION=2.6.5
FROM ruby:$RUBY_VERSION

EXPOSE 4567/tcp

WORKDIR /app

RUN useradd -u 1000 -Um middleman && \
    chown middleman:middleman /app && \
    gem install bundler

COPY --chown=middleman:middleman Gemfile /app
COPY --chown=middleman:middleman Gemfile.lock /app

RUN bundle install

USER middleman

CMD ["/bin/bash"]
