#!/bin/bash
docker run -it --rm --name middleman -p 3001:4567 -v ${PWD}:/app registry.gitlab.com/pspc-dsb/myhrapp-pspc-editor/middleman $@
